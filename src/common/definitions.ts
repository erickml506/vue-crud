export type User = {
  id: number;
  firstName: string;
  lastName: string;
  age?: number | null;
};

export type Users = {
  [id: number]: User;
};

export type Row<T> = {
  item: T;
  index: number;
};

export type State = {
  users: Users;
};

export type FormActions = 'edit' | 'add';
