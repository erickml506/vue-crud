import { Component, Vue, Prop } from 'vue-property-decorator';
import { mapGetters } from 'vuex';
import { ADD_USER, EDIT_USER, USERS_ARRAY } from '@/store/values';
import { User, FormActions } from '@/common/definitions';
import router from '@/router';

const DEFAULT_DATA_FORM = {
  firstName: '',
  lastName: '',
  age: ''
};

@Component({
  computed: mapGetters([USERS_ARRAY])
})
export default class UserForm extends Vue {
  @Prop(Number)
  public readonly userId!: number;

  @Prop({ type: String, required: true })
  public readonly action!: FormActions;

  formUser = {
    ...DEFAULT_DATA_FORM
  };

  private get isEditing(): boolean {
    return this.action === 'edit';
  }

  private get isAdding(): boolean {
    return this.action === 'add';
  }

  created(): void {
    if (this.isEditing) {
      const user = this.$store.getters[USERS_ARRAY].filter(
        (item: User) => item.id === this.userId
      )[0];
      if (!user) {
        this.$router.back();
        return;
      }
      this.formUser.firstName = user.firstName;
      this.formUser.lastName = user.lastName;
      this.formUser.age = user.age;
    }
  }

  onSubmit(): void {
    if (this.isEditing) {
      this.$store.commit(EDIT_USER, {
        ...this.formUser,
        ...(this.userId ? { id: this.userId } : {})
      });
    } else if (this.isAdding) {
      this.$store.commit(ADD_USER, this.formUser);
    }

    this.formUser = {
      ...DEFAULT_DATA_FORM
    };
  }
}
