import { Component, Vue } from 'vue-property-decorator';
import { User } from '@/common/definitions';
import { DELETE_USER, USERS_ARRAY } from '@/store/values';
import { mapMutations, mapGetters } from 'vuex';
import router from '@/router';
import { VNode } from 'vue';

@Component({
  computed: mapGetters({
    userArray: USERS_ARRAY
  }),
  methods: mapMutations({
    deleteUser: DELETE_USER
  })
})
export default class UsersList extends Vue {
  deleteUser!: Function;

  userArray!: Array<User>;

  fields = [
    { key: 'firstName', sortable: true },
    { key: 'lastName', sortable: true },
    { key: 'age', sortable: true },
    { key: 'actions', label: 'Actions' }
  ];

  mounted(): void {
    console.log('UsersList mounted', this.users);
  }

  async edit(user: User): Promise<void> {
    await router.push({
      name: 'EditUser',
      params: {
        id: `${user.id}`
      }
    });
  }

  remove(user: User): void {
    this.deleteUser(user.id);
  }

  get users(): Array<User> {
    return this.userArray;
  }

  render(): VNode {
    return (
      <table id="table">
        <thead>
          <th>firstName</th>
          <th>lastName</th>
          <th>age</th>
        </thead>
        <tbody>
          {this.users.map((user: User) => (
            <tr>
              <td>{user.firstName}</td>
              <td>{user.lastName}</td>
              <td>{user.age}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}
