import { VueConstructor } from 'vue/types/vue';
import { PluginFunction, PluginObject } from 'vue/types/plugin';

class Plugins {
  private instance: VueConstructor;

  constructor(instance: VueConstructor) {
    this.instance = instance;
  }

  load(plugins: Array<PluginFunction<object> | PluginObject<object>>) {
    plugins.forEach(plugin => {
      this.instance.use(plugin);
    });
  }
}

export default Plugins;
