import Vue, { CreateElement, VNode } from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Plugins from '@/libs/Plugins';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.use(VueAxios, axios);

new Plugins(Vue).load([BootstrapVue, IconsPlugin]);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h: CreateElement): VNode => h(App)
}).$mount('#app');
