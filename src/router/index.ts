import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/add-user',
    name: 'AddUser',
    component: () => import('@/views/user/Add.vue')
  },
  {
    path: '/edit-user/:id',
    name: 'EditUser',
    component: () => import('@/views/user/Edit.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
