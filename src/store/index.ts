import Vue from 'vue';
import Vuex from 'vuex';
import { State, User } from '@/common/definitions';
import { DELETE_USER, USERS_ARRAY, ADD_USER, EDIT_USER, FETCH_USERS } from './values';
import users from './users.json';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store<State>({
  state: {
    users
  },
  getters: {
    [USERS_ARRAY]: (state: State) => Object.values(state.users)
  },
  mutations: {
    [FETCH_USERS]() {
      Vue.ax
    },
    [ADD_USER](state: State, user: User) {
      const generatedUserId =
        parseInt(
          Object.keys(state.users)
            .sort()
            .slice(-1)[0],
          10
        ) + 1;

      Vue.set(state.users, generatedUserId, {
        id: generatedUserId,
        ...user
      });
    },
    [EDIT_USER](state: State, user: User) {
      Vue.set(state.users, user.id, user);
    },
    [DELETE_USER](state: State, id: number) {
      Vue.delete(state.users, id);
      Object.assign(state, {
        users: state.users
      });
    }
  },
  actions: {},
  modules: {}
});
