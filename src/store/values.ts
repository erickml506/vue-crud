// Mutations
export const DELETE_USER = 'DELETE_USER';
export const ADD_USER = 'ADD_USER';
export const EDIT_USER = 'EDIT_USER';
export const FETCH_USERS = 'FETCH_USERS';

// getters
export const USERS_ARRAY = 'USERS_ARRAY';
