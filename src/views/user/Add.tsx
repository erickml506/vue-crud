import { Component, Vue } from 'vue-property-decorator';
import UserForm from '@/components/UserForm/index';

@Component({ components: { UserForm } })
export default class AddUser extends Vue {
  render() {
    return <UserForm />;
  }
}
